import * as React from "react";
import { motion } from "framer-motion";

export default function Skills() {
    return (
    <section id="skills">
        <motion.div
        id="hero"
        transition={{ ease: "easeInOut", delay: 0.1 }}
        initial={{ opacity: 0, x: 0 }}
        animate={{ opacity: 1, x: 20 }}
        >
            <div className="text-center">
                <h1>Front-End Skills</h1>
            </div>
        </motion.div>
    </section>
    )
}
