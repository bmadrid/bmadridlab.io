import * as React from "react";

export default function AboutMe() {
    return (
        <section id="About">
            <h1 style={{color: `black`}}>About me</h1>
            <p>I am a full stack software developer passionate about continuously learning and growing.
                Even as a child I was spending a lot of my time keeping up with the latest technology news.
                During high school I picked up some coding and was instantly hooked on the problem solving process involved.
            </p>
            <p>
                I was unable to continue my coding journey; however, I decided to get professional training
                in full stack development with Hack Reactor. I was able to learn many important concepts and
                bring my creativity and enthusiasm to every challenge.
            </p>
            <p>
                When I am not coding, I am typically skateboarding, testing out a new recipe, or playing a competitive, team-based
                game such as Valorant or League of Legends.
            </p>
            <a className="text-md outline m-1.5 p-1 text-emerald-700 hover:text-black" href="Bradley Madrid Resume.pdf" title="View my resume" target="_blank">Resume</a>
        </section>
    )
}
