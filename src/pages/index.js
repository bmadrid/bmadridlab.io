import * as React from "react"
import { motion } from "framer-motion"

import { AnchorLink } from "gatsby-plugin-anchor-links"
import Layout from "../components/layout"
import Seo from "../components/seo"
import AboutMe from "../components/about"
import Skills from "../components/skills"

const IndexPage = () => {

  return (
    <Layout>
      <Seo title="Bradley Madrid" />
      <motion.div
        id="hero"
        transition={{ ease: "easeInOut", delay: 0.1 }}
        initial={{ opacity: 0, x: 0 }}
        animate={{ opacity: 1, x: 20 }}
        style={{ textAlign: `center-left`, paddingLeft: `20px` }}>
        <div style={{ display: `inline-block` }}>
          <h1 style={{ marginBottom: `0px` }}>Hi, I'm <span className="text-emerald-700">Bradley.</span></h1>
          <h1>I'm a full-stack developer.</h1>
        </div>
        <p>
          <AnchorLink
            className="text-2xl outline m-1.5 p-1 text-emerald-700 hover:text-black"
            to="/#Skills"
            title="Go to skills section"
          >Skills</AnchorLink>
          <AnchorLink
            className="text-2xl outline m-1.5 p-1 text-emerald-700 hover:text-black"
            to="/#Projects"
            title="Go to projects section"
          >Projects</AnchorLink>
          <AnchorLink
            className="text-2xl outline m-1.5 p-1 text-emerald-700 hover:text-black"
            to="/#About"
            title="Go to about section"
          >About</AnchorLink>
          <AnchorLink
            className="text-2xl outline m-1.5 p-1 text-emerald-700 hover:text-black"
            to="/#Contact"
            title="Go to contact section"
          >Contact</AnchorLink>
        </p>
      </motion.div>
      <Skills />

        <AboutMe />

    </Layout>
  )
}

export default IndexPage
